import { Injectable } from '@nestjs/common';
import { Game } from 'src/games/games.entity';
import videoGames from 'src/utils/load-games';
import { generateNRandomNumbers } from 'src/utils/numbersGenerator';

@Injectable()
export class GenresService {
  /**
   * Obtiene 3 videojuegos aleatorios que pertenezcan al género de videojuegos ingresado
   * @param genreName String con el nombre del género de videojuegos
   * @returns {Game[]} Arreglo con 3 videojuegos
   */
  getThreeRandomGamesByGenre(genreName: string): Game[] {
    const arrGamesByGenre = this.getAllGamesArray().filter((game) =>
      game.genres.some(
        (genre) => genre.toLowerCase() === genreName.toLowerCase(),
      ),
    );

    return generateNRandomNumbers(arrGamesByGenre.length, 3).map(
      (randomNumber) => arrGamesByGenre[randomNumber],
    );
  }

  /**
   * Obtiene todos los videojuegos
   * @returns {Game[]} Arreglo con todos los videojuegos
   */
  private getAllGamesArray() {
    let allGamesArray = [];
    for (let key in videoGames) {
      allGamesArray = Object.assign(allGamesArray, videoGames[key]);
    }
    return allGamesArray;
  }
}
