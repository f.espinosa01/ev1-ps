import { Body, Controller, Post } from '@nestjs/common';
import { GenresService } from './genres.service';
import { transformResponse } from 'src/utils/transformResponse';

@Controller('genres')
export class GenresController {
  constructor(private genresService: GenresService) {}

  /**
   * Obtiene un arreglo con 3 videojuegos aleatorios que pertenezcan al género de videojuegos ingresado
   * @param genreName String con el nombre del género de videojuegos
   * @returns {string[]} Arreglo con 3 videojuegos
   */
  @Post('random_games')
  getThreeRandomGamesByGenre(@Body('genre_name') genreName) {
    return {
      reponse: transformResponse(
        this.genresService.getThreeRandomGamesByGenre(genreName),
      ),
    };
  }
}
