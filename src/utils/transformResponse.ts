import { Game } from 'src/games/games.entity';

/**
 * Transforma un array de objetos de tipo 'Game' en un array de strings con un formato específico.
 * @param {Game[]} array - El array de objetos de tipo 'Game' a transformar.
 * @returns {string[]} - Un array de strings con el formato deseado.
 */
export function transformResponse(array: Game[]): string[] {
  return array.map((game) => {
    const genreString = game.genres.join(', '); // Une los géneros en una cadena
    return `${game.name} - ${game.video_console} - ${genreString}`;
  });
}
