/**
 * Genera un número aleatorio entre 0 (inclusive) y el valor máximo especificado (excluyente).
 * @param {number} maxRange - El valor máximo para el rango de números aleatorios.
 * @returns {number} - El número aleatorio generado.
 */
export function generateRandomNumber(maxRange: number): number {
  const indiceAleatorio = Math.floor(Math.random() * maxRange);
  return indiceAleatorio;
}

/**
 * Genera un arreglo de números aleatorios.
 * @param {number} nQuantity - La cantidad de números aleatorios a generar.
 * @param {number} maxRange - El valor máximo para el rango de números aleatorios.
 * @returns {number[]} - Un arreglo de números aleatorios.
 */
export function generateNRandomNumbers(
  nQuantity: number,
  maxRange: number,
): number[] {
  let randomNumbers: number[] = [];

  for (let i = 0; i < nQuantity; i++) {
    do {
      var randomNumber = generateRandomNumber(maxRange);
    } while (randomNumbers.includes(randomNumber));
    randomNumbers.push(randomNumber);
  }

  return randomNumbers;
}
