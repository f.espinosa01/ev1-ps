/**
 * Verifica si una propiedad específica existe en un objeto dado.
 * @param {any} objeto - El objeto en el que se verificará la propiedad.
 * @param {string} nombrePropiedad - El nombre de la propiedad que se desea verificar.
 * @returns {boolean} - True si la propiedad existe en el objeto, de lo contrario, false.
 */
export function verificarPropiedad(objeto: any, nombrePropiedad: string): boolean {
  // Verificar si la propiedad existe en el objeto
  return nombrePropiedad in objeto;
}
