import GBA from '../../data/GBA.json';
import N64 from '../../data/N64.json';
import PS2 from '../../data/PS2.json';

const videoGames = {
  PS2,
  GBA,
  N64,
};

export default videoGames;
