import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConsolesModule } from './consoles/consoles.module';
import { GenresModule } from './genres/genres.module';
import { GamesModule } from './games/games.module';

@Module({
  imports: [ConsolesModule, GenresModule, GamesModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
