import { Injectable } from '@nestjs/common';
import * as fs from 'fs';

@Injectable()
export class GamesService {
    private readonly data: any;
    
    /**
     * @description constructor de la clase, este extrae los datos de los archivo json 
     * en la carpeta data
     */
    constructor() {
        const gbaData = JSON.parse(fs.readFileSync('data/GBA.json', 'utf8'));
        const n64Data = JSON.parse(fs.readFileSync('data/N64.json', 'utf8'));
        const ps2Data = JSON.parse(fs.readFileSync('data/PS2.json', 'utf8'));
        this.data = [...gbaData, ...n64Data, ...ps2Data];
    }

    /**
     * @description busca juegos por nombre
     * @param {string} name nombre del juego
     * @returns {Array} Array de el tipo de consola y genero del juego
     */
    findByName(name: string): any{
        const game = this.data.find((game: any) => game.name.toLowerCase().replace(/'/g, '')  === name.toLowerCase().replace(/"/g, '') );
        const result = game ? { response: `${game.video_console} - ${game.genres}`}: { response: 'Juego no encontrado' };
        return result;
    }
}
