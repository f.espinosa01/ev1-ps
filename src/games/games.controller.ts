import { Controller, Get, Query} from '@nestjs/common';
import { GamesService } from './games.service';
  /**
   * @description busca juegos por nombre
   * @param {string} name nombre del juego
   * @returns {Array} Array de el tipo de consola y genero del juego
   */
@Controller('game')
export class GamesController {
    constructor (private readonly gameService: GamesService) {}
    @Get()
    async findByName(@Query('name') name: string){
        return await this.gameService.findByName(name);
    }
}
