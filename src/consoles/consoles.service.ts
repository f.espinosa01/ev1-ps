import { Injectable } from '@nestjs/common';
import videoGames from '../../src/utils/load-games';
import { generateNRandomNumbers } from '../utils/numbersGenerator';
import { transformResponse } from '../utils/transformResponse';
import { verificarPropiedad } from 'src/utils/validateData';
@Injectable()
export class ConsolesService {
  /**
   ** @param console_abreviation tipo de consola de la cual se generaran los juegos de forma aleatoria
   * @returns {Object[]} Array de objetos tipo juego generados de forma aleatoria
   */
  getRandomsGames(console_abreviation: string): Object[] {
    const randomNumbers = generateNRandomNumbers(
      3,
      videoGames[console_abreviation].length,
    );
    const randomGames = randomNumbers.map((randomNumber) => {
      return videoGames[console_abreviation][randomNumber];
    });
    return transformResponse(randomGames);
  }

  /**
   * Ejecuta la obtención de juegos aleatorios para una consola específica.
   * @param {string} console_abreviation La abreviatura de la consola para la que se desean juegos aleatorios.
   * @returns Un arreglo de objetos que representan juegos aleatorios para la consola especificada.
   */
  executeGetRandomGames(console_abreviation: string): Object[] {
    if (verificarPropiedad(videoGames, console_abreviation.toUpperCase())) {
      return this.getRandomsGames(console_abreviation.toUpperCase());
    } else {
      return [];
    }
  }
}
