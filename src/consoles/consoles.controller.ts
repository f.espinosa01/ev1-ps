import { Controller, Get, Param, Post, Body } from '@nestjs/common';
import { ConsolesService } from './consoles.service';
import { transformResponse } from 'src/utils/transformResponse';

@Controller('consoles')
export class ConsolesController {
  constructor(private consolesService: ConsolesService) {}

  /**
   * @description Recomienda dos juegos aleatorios según una consola
   * @param consoleAbreviation String asociado a una abreviación de consola asociado a una abreviación de consola
   * @returns {Array} Array de juegos asociados a la consola
   */
  @Get(':console_abreviation/random_games')
  getTwoRandomGamesByConsole(@Param('console_abreviation') consoleAbreviation) {
    return {
      reponse: `Here are two random games for the ${consoleAbreviation} console`,
      games: this.consolesService.executeGetRandomGames(
        consoleAbreviation.toUpperCase(),
      ),
    };
  }

  /**
   * Obtiene 1 juego aleatorio para una consola y género específico
   * @param consoleAbreviation String con la abreviatura del nombre de consola
   * @param genreName String con el nombre del género
   * @returns respuesta con un juego aleatorio
   */

  @Post(':console_abreviation/genre/random_game')
  getOneRandomGameByConsoleAndGenre(
    @Param('console_abreviation') consoleAbreviation,
    @Body('genre_name') genreName,
  ) {
    return {
      reponse: `Here is a random game for the ${consoleAbreviation} console and the ${genreName} genre`,
    };
  }
}
