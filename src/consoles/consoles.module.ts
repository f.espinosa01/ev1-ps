import { Module } from '@nestjs/common';
import { ConsolesController } from './consoles.controller';
import { ConsolesService } from './consoles.service';

@Module({
  controllers: [ConsolesController],
  providers: [ConsolesService]
})
export class ConsolesModule {}
